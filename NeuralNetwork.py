# -*- coding: utf-8 -*-

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset
from torch.utils.data import DataLoader

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from collections import Counter
from sklearn.metrics import confusion_matrix

from DataParser import DataParser
from DataConverter import DataConverter

import os
os.environ["KMP_DUPLICATE_LIB_OK"]="TRUE"
    
class NeuralNetwork(nn.Module):
    """
    Architecture of the neural network.
    Inherit from torch.nn.Module
    
    Attributes
    ----------
    hidden_layers : list of nn.Linear
        List of the hidden layers of the neural network.
        
    input : nn.Linear
        Input layer of the neural network.

    output : nn.Linear
        Output layer of the neural network.
    """
    
    
    def __init__(self, input_size = 62, output_size = 2, hidden_layers_sizes = [25,25,10]):
        """Constructor of the NeuralNetwork class
        
        Constructor of the NeuralNetwork class which has `input_size`, 
        `output_size` and `hidden_layers_sizes` has optional parameters.

        Parameters
        ----------
        input_size : int, optional
            The size of the input layer. The default is 62.
        output_size : int, optional
            The size of the output layer. The default is 2.
        hidden_layers_sizes : list of int, optional
            The size of each hidden layer. The default is [25,25,10].
            
        Asserts
        -------
        hidden_layers_sizes must be a list.

        Returns
        -------
        None.

        """
        super(NeuralNetwork, self).__init__()
        
        assert isinstance(hidden_layers_sizes, list), "The parameter size_hidden_layers should be a list"
        nb_hidden_layers = len(hidden_layers_sizes)
        self.hidden_layers = []
        self.input = nn.Linear(input_size, hidden_layers_sizes[0])
        
        if nb_hidden_layers > 1:
            for index_layer in range(nb_hidden_layers-1):
                self.hidden_layers.append(nn.Linear(hidden_layers_sizes[index_layer],hidden_layers_sizes[index_layer+1]))
        
        self.output = nn.Linear(hidden_layers_sizes[-1], output_size)
        
    def forward(self, x):
        """Forward propagation of the neural network.
        
        Forward the input data through the network and return the output data.

        Parameters
        ----------
        x : torch.Tensor
            Input data.

        Returns
        -------
        x : torch.Tensor
            Output data.

        """
        x = self.input(x)
        x = F.relu(x)
        
        for layer in self.hidden_layers:
            x = layer(x)
            x = F.relu(x)
            
        x = self.output(x)
        x = F.softmax(x, dim=-1)
        
        return x
    
class ConvertedData(Dataset):
    """
    Creation of a Dataset usable by a DataLoader.
    Inherit from torch.utils.data.Dataset
    
    Attributes
    ----------
    data : list of list of float
        Converted data as a list.
        
    label : list of int
        Converted label as a list.
        
    class_weights : torch.Tensor
        Weights of each class in label.
    """
    
    def __init__(self, data, label):
        """Constructor of the ConvertedData class.
        
        Constructor of the ConvertedData class which takes the input `data`
        and the `label` to make it usable by a DataLoader.
        

        Parameters
        ----------
        data : list of list of float
            Converted data as a list.
        label : list of int
            Converted label as a list.

        Returns
        -------
        None.

        """
        self.data = data
        self.label = label
        counter_label = Counter(self.label)
        count_label = [counter_label[0], counter_label[1]]
        self.class_weights = torch.from_numpy(len(self.label) / (np.array(count_label))).type(torch.float)
        
    def __len__(self):
        """Get length of `data`
        

        Returns
        -------
        int
            Length of `data`.

        """
        return len(self.data)
        
    def __getitem__(self, index):
        """Get item at index.
        
        Get `data` and `label` at `index` as tensors.
        

        Parameters
        ----------
        index : int
            Index of the data and label to get.

        Returns
        -------
        tensor_data : torch.Tensor
            Tensor corresponding to the data at index.
        tensor_label : torch.Tensor
            Tensor corresponding to the data at label.

        """
        tensor_data = torch.tensor(self.data[index])
        np_label = np.zeros(2)
        np_label[self.label[index]] += 1
        tensor_label = torch.tensor(np_label)
        return tensor_data, tensor_label
        
        
if __name__ == "__main__":
    # Defining the ratio of train, valid and test dataset
    train_ratio = 0.5
    valid_ratio = 0.15
    test_ratio = 0.35
    
    
    # Geting the converted data. 
    dataParser = DataParser("Data/")
    dataParser.parse()
    dataConverter = DataConverter(dataParser)
    # dataConverter.convertData() # This operation can take several hours, comment it if you can load data from a file.
    # dataConverter.saveConvertedData("savedConvertedData.csv") # if previous line is commented, comment it too.
    dataConverter.loadConvertedData("savedConvertedData.csv")
    
    
    # Getting the length of each datasets
    len_data = len(dataConverter.convertedData)
    nb_train_data = int(len_data * train_ratio)
    nb_valid_data = int(len_data * valid_ratio)
    nb_test_data = int(len_data * test_ratio)
    
    
    # Splitting the data into datasets
    train_data = [item[0] for item in dataConverter.convertedData[:nb_train_data]]
    train_label = [item[1] for item in dataConverter.convertedData[:nb_train_data]]
    train_dataset = ConvertedData(train_data, train_label)
    
    valid_data = [item[0] for item in dataConverter.convertedData[nb_train_data:nb_train_data+nb_valid_data]]
    valid_label = [item[1] for item in dataConverter.convertedData[nb_train_data:nb_train_data+nb_valid_data]]
    valid_dataset = ConvertedData(valid_data, valid_label)
    
    test_data = [item[0] for item in dataConverter.convertedData[nb_train_data+nb_valid_data:]]
    test_label = [item[1] for item in dataConverter.convertedData[nb_train_data+nb_valid_data:]]
    test_dataset = ConvertedData(test_data, test_label)
    
    # Getting each Dataloader.
    batch_size = 1000
    train_dataloader = DataLoader(train_dataset, shuffle=True, batch_size=batch_size)
    valid_dataloader = DataLoader(valid_dataset, shuffle=False, batch_size=batch_size)
    test_dataloader = DataLoader(test_dataset, shuffle=False, batch_size=batch_size)
    
    
    # Defining the neural network
    model = NeuralNetwork()
    CELoss = torch.nn.CrossEntropyLoss(weight=train_dataset.class_weights, reduction='mean')
    optimizer = torch.optim.Adam(model.parameters(), lr=0.001, betas=(0.9, 0.999))
    
    # Initializing training variables.
    mean_train_losses = []
    mean_valid_losses = []
    valid_acc_list = []
    epochs = 150
    best_acc = 0
    epoch_best_acc = 0
    
    # Train model
    for epoch in range(epochs):
        model.train()
        
        train_losses = []
        valid_losses = []
        
        for batch, (data, label) in enumerate(train_dataloader):
            optimizer.zero_grad()
            
            output = model(data)
            loss = CELoss(output, label)
            loss.backward()
            optimizer.step()
            
            train_losses.append(loss.item())
            if (batch * batch_size) % (batch_size * 100) == 0:
                print(f'{batch * batch_size} / {nb_train_data}')
                
        model.eval()
        correct = 0
        total = 0
        with torch.no_grad():
            for batch, (data, label) in enumerate(valid_dataloader):
                output = model(data)
                loss = CELoss(output, label)
                
                valid_losses.append(loss.item())
                
                for index in range(len(output)):
                    if torch.argmax(output[index]) == torch.argmax(label[index]):
                        correct += 1
                total += label.size(0)
        
        mean_train_losses.append(np.mean(train_losses))
        mean_valid_losses.append(np.mean(valid_losses))
        
        accuracy = 100*correct/total
        valid_acc_list.append(accuracy)
        print('epoch : {}, train loss : {:.4f}, valid loss : {:.4f}, valid acc : {:.2f}%'\
              .format(epoch+1, np.mean(train_losses), np.mean(valid_losses), accuracy))
            
        if accuracy > best_acc:
            best_acc = accuracy
            index_best_acc = epoch
            torch.save(model, "best_model.pt")
            
            
    print("Best model on the validation dataset at epoch " + str(epoch) + " with " + str(best_acc) + "% accuracy."  )
            
    # Draw train and valid losses graphs.
    fig, (ax1, ax2) = plt.subplots(nrows = 1, ncols = 2, figsize=(15, 10))
    ax1.plot(mean_train_losses, label='train')
    ax1.plot(mean_valid_losses, label='valid')
    lines, labels = ax1.get_legend_handles_labels()
    ax1.legend(lines, labels, loc='best')
    
    ax2.plot(valid_acc_list, label="valid acc")
    ax2.legend()
    
    
    # Evaluate model on test dataset
    model = torch.load("best_model.pt")
    model.eval()
    test_preds = torch.LongTensor()
    
    for batch, (data, label) in enumerate(test_dataloader):
        output = model(data)
        
        pred = output.max(1, keepdim=True)[1]
        test_preds = torch.cat((test_preds, pred), dim=0)
        
    pred_list = test_preds.squeeze().tolist()
    
    
    
    out_df = pd.DataFrame()
    out_df['ID'] = ["Withdrawn/Fail", "Pass/Distinction"]
    count_pred = Counter(pred_list)
    out_df['Pred'] = np.array([count_pred[0], count_pred[1]])
    count_label = Counter(test_label)
    out_df['Label'] = np.array([count_label[0], count_label[1]])
    print(out_df.head())
    
    conf_matrix = confusion_matrix(test_label, pred_list)
    row_labels = ["Real Withdrawn/Fail", "Real Pass/Distinction"]
    columns_labels = ["Predicted Withdrawn/Fail", "Predicted Pass/Distinction"]
    conf_df = pd.DataFrame(conf_matrix, columns = columns_labels, index = row_labels)
    print(conf_df)
    tn, fp, fn, tp = conf_matrix.ravel()
    p = tp + fn
    n = fp + tn
    pp = tp + fp
    pn = fn + tn
    tpr = tp/p
    tnr = tn/n
    for_ = fn/pn
    fdr = fp/pp
    acc_test = (tp + tn) / (p + n) * 100
    print("Accuracy on the test dataset : ", acc_test)
    print("True positive rate :", tpr)
    print("True negative rate :", tnr)
    print("False omission rate :", for_)
    print("False discovery rate :", fdr)
    
    