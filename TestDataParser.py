# -*- coding: utf-8 -*-

import unittest
import sys
from DataParser import DataParser

class TestDataParser(unittest.TestCase):
    
    def test_path_exist_and_is_dir(self):
        """
        Test if `rep_path` attribute exist and is a directory.
        """
        path = "Test/Data"
        dataParser = DataParser(path)
        self.assertTrue(dataParser.existAndIsDir())
        
    def test_path_exist_but_is_not_dir(self):
        """
        Test if `rep_path` attribute exist but is not a directory.
        """
        path = "Test/test.txt"
        dataParser = DataParser(path)
        self.assertFalse(dataParser.existAndIsDir())
        
    def test_path_not_exist(self):
        """
        Test if `rep_path` attribute doesn't exist.
        """
        path = "Test/wrongPath"
        dataParser = DataParser(path)
        self.assertFalse(dataParser.existAndIsDir())
        
    def test_dir_has_csv_files(self):
        """ 
        Test if `rep_path` attribute has all the csv files from OULA.
        """
        path = "Test/Data/AllFiles"
        dataParser = DataParser(path)
        self.assertTrue(dataParser.dirContainsCsvFiles())
        
    def test_dir_has_no_csv_files(self):
        """
        Test if `rep_path` attribute doesn't have all the csv files from OULA.
        """
        path = "Test/Data/NoFiles"
        dataParser = DataParser(path)
        self.assertFalse(dataParser.dirContainsCsvFiles())
        
    def test_all_student_info_indexes(self):
        """
        Test if when having the right field values, the indexes are correct.
        """
        list_line = ["code_module", "code_presentation", "id_student",
                     "random_field", "final_result"]
        indexes = [None] * 4
        expected_indexes = [0,1,2,4]
        
        dataParser = DataParser(" ") #The path isn't needed here.
        indexes = dataParser.getStudentInfoIndexes(list_line, indexes)
        self.assertListEqual(indexes, expected_indexes)
        
    def test_miss_a_student_info_index(self):
        """
        Test if when having a missing field value, one index stay at None.
        """
        list_line = ["code_module", "code_presentation", "id_student", "random_field"]
        indexes = [None] * 4
        expected_indexes = [0,1,2,None]
        
        dataParser = DataParser(" ") #The path isn't needed here.
        indexes = dataParser.getStudentInfoIndexes(list_line, indexes)
        self.assertListEqual(indexes, expected_indexes)
        
    def test_all_student_assessment_indexes(self):
        """
        Test if when having the right field values, the indexes are correct.
        """
        list_line = ["id_assessment", "id_student", "random_field", "score"]
        indexes = [None] * 3
        expected_indexes = [0,1,3]
        
        dataParser = DataParser(" ") #The path isn't needed here.
        indexes = dataParser.getStudentAssessmentIndexes(list_line, indexes)
        self.assertListEqual(indexes, expected_indexes)
        
    def test_miss_a_student_assessment_index(self):
        """
        Test if when having a missing field value, one index stay at None.
        """
        list_line = ["id_assessment", "id_student", "random_field"]
        indexes = [None] * 3
        expected_indexes = [0,1,None]
        
        dataParser = DataParser(" ") #The path isn't needed here.
        indexes = dataParser.getStudentAssessmentIndexes(list_line, indexes)
        self.assertListEqual(indexes, expected_indexes)
        
    def test_all_assessments_indexes(self):
        """
        Test if when having the right field values, the indexes are correct.
        """
        list_line = ["code_module", "code_presentation",
                     "id_assessment", "random_field", "assessment_type"]
        indexes = [None] * 4
        expected_indexes = [0,1,2,4]
        
        dataParser = DataParser(" ") #The path isn't needed here.
        indexes = dataParser.getAssessmentsIndexes(list_line, indexes)
        self.assertListEqual(indexes, expected_indexes)
        
    def test_miss_an_assessments_index(self):
        """
        Test if when having a missing field value, one index stay at None.
        """
        list_line = ["code_module", "code_presentation", "id_assessment", "random_field"]
        indexes = [None] * 4
        expected_indexes = [0,1,2,None]
        
        dataParser = DataParser(" ") #The path isn't needed here.
        indexes = dataParser.getAssessmentsIndexes(list_line, indexes)
        self.assertListEqual(indexes, expected_indexes)
        
        
    def test_all_courses_indexes(self):
        """
        Test if when having the right field values, the indexes are correct.
        """
        list_line = ["code_module", "code_presentation", "random_field", "module_presentation_length"]
        indexes = [None] * 3
        expected_indexes = [0,1,3]
        
        dataParser = DataParser(" ") #The path isn't needed here.
        indexes = dataParser.getCoursesIndexes(list_line, indexes)
        self.assertListEqual(indexes, expected_indexes)
        
    def test_miss_a_courses_index(self):
        """
        Test if when having the right field values, the indexes are correct.
        """
        list_line = ["code_module", "code_presentation", "random_field"]
        indexes = [None] * 3
        expected_indexes = [0,1,None]
        
        dataParser = DataParser(" ") #The path isn't needed here.
        indexes = dataParser.getCoursesIndexes(list_line, indexes)
        self.assertListEqual(indexes, expected_indexes)
        
    def test_student_info_added_when_indexes_ok(self):
        """
        Test if all students info are added to `final_results`
        """
        list_line = ["AAA", "2013J", "11391", "Random Value", "Pass"]
        indexes = [0,1,2,4]
        expected_final_results = [["AAA", "2013J", "11391", "Pass"]]
        
        dataParser = DataParser(" ")
        dataParser.addStudentInfoValues(list_line, indexes)
        self.assertListEqual(expected_final_results, dataParser.final_results)
        
    def test_student_info_error_when_index_none(self):
        """
        Test when an index is missing that an error string is sent.
        """
        list_line = ["AAA", "2013J", "11391", "Random Value", "Pass"]
        indexes = [0,1,2,None]
        expected_string = "Missing final_result index."
        
        dataParser = DataParser(" ")
        dataParser.addStudentInfoValues(list_line, indexes)
        #Code from https://stackoverflow.com/questions/4219717/how-to-assert-output-with-nosetest-unittest-in-python/17981937
        if not hasattr(sys.stdout, "getvalue"):
            self.fail("need to run in buffered mode")
        output = sys.stdout.getvalue().strip() #Because stdout is an StringIO instance
        self.assertEquals(output, expected_string)
        
    def test_student_assessment_added_when_indexes_ok(self):
        """
        Test if all students assessments are added to `scores`
        """
        list_line = ["1752", "11391", "Random Value", "78"]
        indexes = [0,1,3]
        expected_scores = [["1752", "11391", "78"]]
        
        dataParser = DataParser(" ")
        dataParser.addStudentAssessmentValues(list_line, indexes)
        self.assertListEqual(expected_scores, dataParser.scores)
        
    def test_student_assessment_error_when_index_none(self):
        """
        Test when an index is missing that an error string is sent.
        """
        list_line = ["1752", "11391", "Random Value", "78"]
        indexes = [0,1,None]
        expected_string = "Missing score index."
       
        dataParser = DataParser(" ")
        dataParser.addStudentAssessmentValues(list_line, indexes)
        #Code from https://stackoverflow.com/questions/4219717/how-to-assert-output-with-nosetest-unittest-in-python/17981937
        if not hasattr(sys.stdout, "getvalue"):
            self.fail("need to run in buffered mode")
        output = sys.stdout.getvalue().strip() #Because stdout is an StringIO instance
        self.assertEquals(output, expected_string)
        
    def test_assessments_added_when_indexes_ok(self):
        """
        Test if all assessments are added to `assessments_types`
        """
        list_line = ["AAA","2013J", "1752", "Random Value", "TMA"]
        indexes = [0,1,2,4]
        expected_assessments_types = [["AAA", "2013J", "1752", "TMA"]]
        
        dataParser = DataParser(" ")
        dataParser.addAssessmentsValues(list_line, indexes)
        self.assertListEqual(expected_assessments_types, dataParser.assessments_types)
        
    def test_assessments_error_when_index_none(self):
        """
        Test when an index is missing that an error string is sent.
        """
        list_line = ["AAA", "2013J", "1752", "Random Value", "TMA"]
        indexes = [0,1,2,None]
        expected_string = "Missing assessment_type index."
       
        dataParser = DataParser(" ")
        dataParser.addAssessmentsValues(list_line, indexes)
        #Code from https://stackoverflow.com/questions/4219717/how-to-assert-output-with-nosetest-unittest-in-python/17981937
        if not hasattr(sys.stdout, "getvalue"):
            self.fail("need to run in buffered mode")
        output = sys.stdout.getvalue().strip() #Because stdout is an StringIO instance
        self.assertEquals(output, expected_string)
        
    def test_all_courses_added_when_indexes_ok(self):
        """
        Test if all courses are added to `modules_presentations_lengths`
        """
        list_line = ["AAA", "2013J", "Random Value", "268"]
        indexes = [0,1,3]
        expected_modules_presentations_lengths = [["AAA", "2013J", "268"]]
        
        dataParser = DataParser(" ")
        dataParser.addCoursesValues(list_line, indexes)
        self.assertListEqual(expected_modules_presentations_lengths, dataParser.modules_presentations_lengths)
        
    def test_courses_error_when_index_none(self):
        """
        Test when an index is missing that an error string is sent.
        """
        list_line = ["AAA", "2013J", "Random Value", "268"]
        indexes = [0,1,None]
        expected_string = "Missing module_presentation_length index."
       
        dataParser = DataParser(" ")
        dataParser.addCoursesValues(list_line, indexes)
        #Code from https://stackoverflow.com/questions/4219717/how-to-assert-output-with-nosetest-unittest-in-python/17981937
        if not hasattr(sys.stdout, "getvalue"):
            self.fail("need to run in buffered mode")
        output = sys.stdout.getvalue().strip() #Because stdout is an StringIO instance
        self.assertEquals(output, expected_string)
        
    def test_correct_dir_parse_successfully(self):
        """
        Test if the parsing was successfull when given a correct path.
        """
        #Defining expected results.
        expected_first_final_results = ["AAA", "2013J", "11391", "Pass"]
        expected_first_scores = ["1752", "11391", "78"]
        expected_first_assessments_types = ["AAA","2013J", "1752", "TMA"]
        expected_first_modules_presentations_lengths = ["AAA", "2013J", "268"]
        
        path = "Test/Data/AllFiles"
        dataParser = DataParser(path)
        dataParser.parse()
        
        self.assertListEqual(dataParser.final_results[0], expected_first_final_results)
        self.assertListEqual(dataParser.scores[0], expected_first_scores)
        self.assertListEqual(dataParser.assessments_types[0], expected_first_assessments_types)
        self.assertListEqual(dataParser.modules_presentations_lengths[0], expected_first_modules_presentations_lengths)
        
    def test_wrong_dir_does_nothing(self):
        """
        Test if list attributes are empty if wrong path is given.
        """
        path = "Test/Data/NoFiles"
        dataParser = DataParser(path)
        dataParser.parse()
        
        self.assertListEqual(dataParser.final_results, [])
        self.assertListEqual(dataParser.scores, [])
        self.assertListEqual(dataParser.assessments_types, [])
        self.assertListEqual(dataParser.modules_presentations_lengths, [])
    
    
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestDataParser)
    unittest.TextTestRunner(verbosity=0, buffer=True).run(suite)