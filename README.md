# Projet Recherche & Développement


## Objectifs du projet

L'objectif de ce projet est de prédire les étudiants en risques de décrochage dans le milieu universitaire. Pour ce faire ce projet est décomposé en deux parties. 
Une première partie où l'on va entraîner un réseau de neurones sur des données issues d'un MOOC assez proche de données que l'on pourrait avoir en milieu universitaire. L'objectif de cette première partie est de vérifier la faisabilité de la technique sur ce genre de données. 
En cas de faisabilité, la deuxième partie est de mettre en place une application web duquel les notes des étudiants pourraient être receuilli afin d'être utilisé par le réseau de neurones pour prédire les potentiels décrochages. L'ensemble de ces prédictions sont ensuite affichés aux enseignants sous la forme de différentes visualisations, permettant ainsi aux enseignants de mettre en place des actions pour les étudiants prédits comme étant en risque de décrochages.

## Versions et installations
L'ensemble du projet à été réalisé avec les versions suivantes :
- Python 3.8.8, suivre le détail de l'installation depuis : https://www.python.org/downloads/release/python-388/
- PyTorch 1.10.1, suivre le détail de l'installation depuis : https://pytorch.org/get-started/previous-versions/
- Matplotlib 3.3.4, suivre le détail de l'installation depuis : https://matplotlib.org/stable/users/installing/index.html
- NumPy 1.20.1, suivre le détail de l'installation depuis : https://numpy.org/install/
- Pandas 1.2.4, suivre le détail de l'installation depuis : https://pandas.pydata.org/docs/getting_started/install.html
- scikit-learn 0.24.1, suivre le détail de l'installation depuis : https://scikit-learn.org/stable/install.html

## Spécifications fonctionnelles

### Partie réseau de neurones 

### Définition de la fonction 1 : Importation de la base de données

    Présentation de la fonction 1 :
    - Nom de la fonction : Importation de la base de données.
    - Les données de la base OULA doivent être importées pour être traitées.
    - Primordiale.

    Description de la fonction 1 :
    - Cette fonction importe dans le système l'ensemble des fichiers .csv de la base OULA utile à ce projet.
    - Entrée : studentInfo.csv, studentAssessment.csv, assessment.csv, courses.csv
    - Sortie :
        - La liste des résultats finaux des étudiants [code_module, code_presentation, id_student, final_result]
        - La liste des notes des étudiants aux évaluations [id_assessment, id_student, score]
        - La liste des évaluations des présentations des modules avec leurs types d'évaluation [code_module, code_presentation, id_assessment, assessment_type]
        - La liste des présentations des modules avec leurs durées [code_module, code_presentation, module_presentation_length]


### Définition de la fonction 2 : Traitement et transformation des données

    Présentation de la fonction 2 :
    - Nom de la fonction : Traitement et transformation des données
    - item Les données doivent être traitées puis transformées pour quelles correspondent au format de données attendus par le réseau de neurones.
    - item Primordiale.

    Description de la fonction 1 :
    - Cette fonction convertis les données importées en données numériques qui sont ensuite transformées sous la forme d'une liste de liste.
    - Entrée : Les données traités :
        - La liste des résultats finaux des étudiants [code_module, code_presentation, id_student, final_result]
        - La liste des notes des étudiants aux évaluations [id_assessment, id_student, score]
        - La liste des évaluations des présentations des modules avec leurs types d'évaluation [code_module, code_presentation, id_assessment, assessment_type]
        - La liste des présentations des modules avec leurs durées [code_module, code_presentation module_presentation_length]
    - Sortie :
        - La liste des données converties : [converted_code_module, converted_code_presentation, converted_id_student, converted_module_presentation_length, converted_mean_student, converted_global_mean, converted_id_assessment_1, converted_type_assessment_1, converted_score_1, converted_mean_eval_1, ... , converted_id_assessment_14, converted_type_assessment_14, converted_score_14, converted_mean_eval_14]. 


### Définition de la fonction 3: Entraînement et sauvegarde du réseau de neurones

    Présentation de la fonction 3 :
    - Nom de la fonction : Entraînement et sauvegarde du réseau de neurones.
    - Cette fonction entraîne le réseau de neurones sur la base de données OULA.
    - Primordiale.

    Description de la fonction 3 :
    - Le réseau de neurones est entraîné sur les données traitées et transformées de la base OULA. 
    Les données de la base sont divisés en 3 bases. La base d'entraînement, la base de validation et la base de test. 
    Le réseau de neurones n'est sauvegardé que lorsque le pourcentage de bonne prédiction sur la base de validation dépasse le pourcentage du précédent modèle.
    - Entrée : La liste des données converties : [converted_code_module, converted_code_presentation, converted_id_student, converted_module_presentation_length, converted_mean_student, converted_global_mean, converted_id_assessment_1, converted_type_assessment_1, converted_score_1, converted_mean_eval_1, ... , converted_id_assessment_14, converted_type_assessment_14, converted_score_14, converted_mean_eval_14].
    - Sortie : Le modèle ayant la meilleure précision sur la base de validation, et sa précision sur la base de test.

### Partie plateforme numérique

### Définition de la fonction 4: Authentification

    Présentation de la fonction 4 :
    - Nom de la fonction : Authentification.
    - Les utilisateurs doivent pouvoir se connecter afin d'accéder à leur vue des prédictions. 
    - Primordiale

    Description de la fonction 4 :
    - Cette fonction permet aux utilisateurs de se connecter. Ces derniers ne peuvent avoir accès à la vue qui leur est associé que si ils sont connectés. 
    Les étudiants peuvent voir les prédiction pour chacun des modules auxquels ils sont inscrits. 
    Comme aucun enseignants n'est associés aux modules dans la base, on considérera qu'un module représente un enseignant, et lorsque l'on se connecte avec ce numéro de module, 
    on a donc accès à l'ensemble des visualisations des étudiants de ce module. 
    Dans le cadre de ce projet un mot de passe unique sera defini pour l'ensemble des utilisateurs.
    - Entrée : L'identifiant de l'étudiant ou du module, et son mot de passe.
    - Sortie : Redirection vers la page de visualisation associé à cet identifiant.
    - Pré-condition : Le système a accès aux données cryptés des identifiants et de leurs mots de passe associés.

### Définition de la fonction 5: Importation des données

    Présentation de la fonction 5 :
    - Nom de la fonction : Importation des données
    - Les données issues des prédictions doivent être importées afin de réaliser la visualisation.
    - Primordiale

    Description de la fonction 5 :
    - Cette fonction permet de récupérer les données des prédictions, ainsi que les données annexes nécessaire à la visualisation. 
    - Entrée : Les données issues des prédictions. Les données annexes, nottament code_module, id_student, id_assessment, score.
    - Sortie : Rien

### Définition de la fonction 6: Visualisation des prédictions

    Présentation de la fonction 6 :
    - Nom de la fonction : Visualisation des prédictions
    - Les données sont utilisées pour réaliser les visualisations.
    - Primordiale

    Description de la fonction 6 :
    - Cette fonction permet d'afficher les visualisations associés à un utilisateur. 
    - Entrée : Les données issues des prédictions et les données annexes nécessaire à la visualisation.
    - Sortie : L'affichage des visualisations.
